DMD project web interface
==========================

API Reference
--------------------
Please see the [Google Drive document](https://docs.google.com/document/d/18NMuKRzGrNf9Cx312hprry7H4xuEXQkiHSycN8ZyHRs/edit?usp=sharing).

Environment Setup and Launch
--------------------
The project is built on the top of Scala and Play Framework 2 with PostgreSQL database. In order to launch it you need to have access to a bash terminal, preferably on Linux. So what you need to do is to clone the repo (assuming you have a BitBucket account with a valid SSH key):
```sh
$ git clone git@bitbucket.org:ariser/dmdweb.git
```

You also should have PostgreSQL installed. Create a database named "dmd_project" and set password for the user "postgres" to "admin". Alternatively, you can adjust the database connection configuration by editing the file /conf/application.conf, lines from 38 to 40.

Then go to the project dir and run the Play Framework activator:
```sh
$ cd dmdtest
$ ./activator
...
[dmdweb]$ compile
[dmdweb]$ run
```
The activator should then create a local server on the port 9000, and the project will be available on the [localhost](http://localhost:9000/).

**Please note** that the first launch most likely will require 15+ minutes to download and resolve all framework dependencies.

Use "admin" as both login and password to get access to the protected area.