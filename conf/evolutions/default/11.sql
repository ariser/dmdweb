# remove redundant attr from types, add rating to publiactions

# --- !Ups

ALTER TABLE publication ADD COLUMN rating SMALLINT;

ALTER TABLE types DROP COLUMN type;

# --- !Downs

ALTER TABLE publication DROP COLUMN rating;

ALTER TABLE types ADD COLUMN type VARCHAR(30);
