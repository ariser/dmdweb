# change ints lo longs

# --- !Ups

ALTER TABLE publication_type ALTER COLUMN id TYPE BIGINT;

ALTER TABLE users ALTER COLUMN id TYPE BIGINT;


# --- !Downs

ALTER TABLE publication_type ALTER COLUMN id TYPE INT;

ALTER TABLE users ALTER COLUMN id TYPE INT;
