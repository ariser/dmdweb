# change ints lo longs

# --- !Ups

ALTER TABLE publication DROP COLUMN type;
ALTER TABLE publication ADD COLUMN type INTEGER;

# --- !Downs

ALTER TABLE publication DROP COLUMN type;
ALTER TABLE publication ADD COLUMN type VARCHAR(30);