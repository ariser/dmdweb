# --- !Ups
CREATE TABLE publication_type (id SERIAL, type_name VARCHAR(30));


# --- !Downs
DROP TABLE publication_type;