# change ints lo longs

# --- !Ups

ALTER TABLE affiliates ADD COLUMN start_date DATE;
ALTER TABLE affiliates ADD COLUMN end_date DATE;

# --- !Downs

ALTER TABLE affiliates DROP COLUMN start_date;
ALTER TABLE affiliates DROP COLUMN end_date;