# fix publication column type, add title for types

# --- !Ups

ALTER TABLE publication ALTER COLUMN type TYPE BIGINT;

ALTER TABLE types ADD COLUMN title VARCHAR(30);
UPDATE types SET title = 'Journal article' WHERE type = 'journal_article';
UPDATE types SET title = 'Conference proceeding' WHERE type = 'conference_proceeding';
UPDATE types SET title = 'Book' WHERE type = 'book';

# --- !Downs

ALTER TABLE publication ALTER COLUMN type TYPE INT;

ALTER TABLE types DROP COLUMN title;
