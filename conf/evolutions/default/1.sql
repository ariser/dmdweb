# initial dump

# --- !Ups

CREATE TABLE institution (
  id   SERIAL PRIMARY KEY,
  name VARCHAR(100),
  city VARCHAR(50)
);

CREATE TABLE author (
  id             SERIAL PRIMARY KEY,
  name           VARCHAR(50),
  academic_title VARCHAR(10)
);

CREATE TABLE publisher (
  id    SERIAL PRIMARY KEY,
  title VARCHAR(100)
);

CREATE TABLE publication (
  id       SERIAL PRIMARY KEY,
  type     VARCHAR(20),
  abstract TEXT,
  doi      VARCHAR(50),
  title    VARCHAR(200)
);

CREATE TABLE keyword (
  id   SERIAL PRIMARY KEY,
  name VARCHAR(30)
);

CREATE TABLE affiliates (
  institution_id INT REFERENCES institution (id),
  author_id      INT REFERENCES author (id)
);

CREATE TABLE book (
  publication_id INT NOT NULL REFERENCES publication (id),
  isbn           VARCHAR(20),
  publisher      INT REFERENCES publisher (id)
);

CREATE TABLE conference (
  id        SERIAL PRIMARY KEY,
  title     VARCHAR(100),
  date      DATE,
  issn      VARCHAR(20),
  publisher INT REFERENCES publication (id)
);

CREATE TABLE conference_contains (
  conference_id  INT REFERENCES conference (id),
  publication_id INT REFERENCES publication (id),
  start_page     INT,
  end_page       INT
);

CREATE TABLE magazine (
  id            SERIAL PRIMARY KEY,
  title         VARCHAR(100),
  history_start DATE,
  history_end   DATE,
  impact        REAL,
  issn          VARCHAR(20),
  publisher     INT REFERENCES publisher (id)
);

CREATE TABLE magazine_contains (
  magazine_id    INT REFERENCES magazine (id),
  publication_id INT REFERENCES publication (id),
  volume         INT,
  issue          INT,
  start_page     INT,
  end_page       INT
);

CREATE TABLE relates (
  keyword_id     INT NOT NULL REFERENCES keyword (id),
  publication_id INT NOT NULL REFERENCES publication (id),
  PRIMARY KEY (keyword_id, publication_id)
);

CREATE TABLE works_with (
  author_1_id INT NOT NULL REFERENCES author (id),
  author_2_id INT NOT NULL REFERENCES author (id),
  PRIMARY KEY (author_1_id, author_2_id)
);

CREATE TABLE writes (
  author_id      INT NOT NULL REFERENCES author(id),
  publication_id INT NOT NULL REFERENCES publication (id),
  PRIMARY KEY (author_id, publication_id)
);


# --- !Downs

DROP TABLE institution CASCADE;
DROP TABLE author CASCADE;
DROP TABLE affiliates CASCADE;
DROP TABLE book CASCADE;
DROP TABLE conference CASCADE;
DROP TABLE conference_contains CASCADE;
DROP TABLE keyword CASCADE;
DROP TABLE magazine CASCADE;
DROP TABLE magazine_contains CASCADE;
DROP TABLE publication CASCADE;
DROP TABLE publisher CASCADE;
DROP TABLE relates CASCADE;
DROP TABLE works_with CASCADE;
DROP TABLE writes CASCADE;