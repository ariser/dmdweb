# add publication types

# --- !Ups

CREATE TABLE types (id BIGSERIAL, type VARCHAR(30));
INSERT INTO types (type) VALUES ('journal_article');
INSERT INTO types (type) VALUES ('conference_proceeding');
INSERT INTO types (type) VALUES ('book');

ALTER TABLE magazine_contains ADD COLUMN year DATE;
ALTER TABLE conference_contains ADD COLUMN year DATE;

# --- !Downs

DROP TABLE types;

ALTER TABLE magazine_contains DROP COLUMN year;
ALTER TABLE conference_contains DROP COLUMN year;