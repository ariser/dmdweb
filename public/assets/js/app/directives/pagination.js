(function() {
  'use_strict';
  angular.module('dmdApp').directive('dmdPager', function() {
    return {
      restrict: 'AE',
      replace: false,
      templateUrl: 'pagination.html',
      controller: 'dmdPaginationCtrl'
    };
  });

}).call(this);

//# sourceMappingURL=../../app/directives/pagination.js.map