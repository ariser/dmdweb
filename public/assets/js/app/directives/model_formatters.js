(function() {
  'use strict';
  angular.module('dmdApp').directive('dmdArrayInput', function() {
    return {
      restrict: 'A',
      require: 'ngModel',
      link: function(scope, elem, attrs, ngModel) {
        ngModel.$formatters.push(function(value) {
          var ref;
          return (ref = value != null ? typeof value.join === "function" ? value.join(',') : void 0 : void 0) != null ? ref : value;
        });
        return ngModel.$parsers.push(function(value) {
          if (value.toString()) {
            return value.toString().split(',');
          } else {
            return [];
          }
        });
      }
    };
  });

}).call(this);

//# sourceMappingURL=../../app/directives/model_formatters.js.map