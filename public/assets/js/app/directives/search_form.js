(function() {
  'use_strict';
  angular.module('dmdApp').directive('publicationSearch', function() {
    return {
      restrict: 'AE',
      replace: true,
      templateUrl: 'search_form.html',
      controller: 'dmdSearchFormCtrl'
    };
  });

}).call(this);

//# sourceMappingURL=../../app/directives/search_form.js.map