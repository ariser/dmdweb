(function() {
  'use_strict';
  angular.module('dmdApp').directive('publicationForm', function() {
    return {
      restrict: 'AE',
      replace: true,
      templateUrl: 'publication_form.html',
      controller: 'dmdPublicationFormCtrl',
      scope: {
        submit: '=',
        form: '=?'
      }
    };
  });

}).call(this);

//# sourceMappingURL=../../app/directives/publication_form.js.map