(function() {
  'use_strict';
  angular.module('dmdApp').directive('deleteButton', function() {
    return {
      restrict: 'AE',
      replace: true,
      templateUrl: 'delete_button.html',
      controller: 'dmdDeleteButtonCtrl',
      scope: {
        id: '=',
        afterDelete: '&'
      }
    };
  });

}).call(this);

//# sourceMappingURL=../../app/directives/delete_button.js.map