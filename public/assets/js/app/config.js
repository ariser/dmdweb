(function() {
  'use strict';
  angular.module('dmdApp').run([
    '$rootScope', '$window', function($rootScope, $window) {
      return $rootScope.$back = function() {
        return $window.history.back();
      };
    }
  ]).config([
    '$httpProvider', function($httpProvider) {
      return $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    }
  ]);

}).call(this);

//# sourceMappingURL=../app/config.js.map