(function() {
  'use_strict';
  angular.module('dmdApp').config([
    '$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
      $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
      });
      return $routeProvider.when('/', {
        templateUrl: 'index.html',
        controller: 'dmdIndexCtrl'
      }).when('/publications', {
        templateUrl: 'list.html',
        controller: 'dmdListCtrl'
      }).when('/publications/:id', {
        templateUrl: 'publication.html',
        controller: 'dmdPublicationCtrl'
      }).when('/publications/:id/edit', {
        templateUrl: 'edit_publication.html',
        controller: 'dmdEditPublicationCtrl'
      }).when('/publications/:id/related', {
        templateUrl: 'related_publications.html',
        controller: 'dmdRelatedPublicationsCtrl'
      }).when('/add', {
        templateUrl: 'add_publication.html',
        controller: 'dmdAddPublicationCtrl'
      }).otherwise({
        redirectTo: '/'
      });
    }
  ]);

}).call(this);

//# sourceMappingURL=../app/routes.js.map