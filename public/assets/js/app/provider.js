(function() {
  'use strict';
  angular.module('dmdApp').factory('dmdProvider', [
    '$http', function($http) {
      var DMDProvider;
      DMDProvider = (function() {
        function DMDProvider() {}

        DMDProvider.prototype.fetchKeywords = function() {
          return $http.get("/keywords");
        };

        DMDProvider.prototype.fetchTypes = function() {
          return $http.get('/paper_types');
        };

        DMDProvider.prototype.find = function(data) {
          return $http.get("/publications", {
            params: data
          });
        };

        DMDProvider.prototype.add = function(data) {
          return $http.post("/publications", data);
        };

        DMDProvider.prototype.fetch = function(id) {
          return $http.get("/publications/" + id);
        };

        DMDProvider.prototype.update = function(id, data) {
          return $http.put("/publications/" + id, data);
        };

        DMDProvider.prototype["delete"] = function(id) {
          return $http["delete"]("/publications/" + id);
        };

        DMDProvider.prototype.findRelated = function(id) {
          return $http.get("/publications/" + id + "/related");
        };

        return DMDProvider;

      })();
      return new DMDProvider;
    }
  ]);

}).call(this);

//# sourceMappingURL=../app/provider.js.map