(function() {
  'use_strict';
  angular.module('dmdApp').controller('dmdPaginationCtrl', [
    '$scope', '$location', function($scope, $location) {
      $scope.prev = function() {
        return $location.search('offset', Math.max($scope.offset - $scope.limit, 0));
      };
      return $scope.next = function() {
        return $location.search('offset', $scope.offset + $scope.limit);
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=../../app/controllers/pagination.js.map