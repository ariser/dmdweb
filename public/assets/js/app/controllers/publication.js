(function() {
  'use_strict';
  angular.module('dmdApp').controller('dmdPublicationCtrl', [
    '$scope', '$routeParams', '$timeout', 'uScope', 'dmdProvider', function($scope, $routeParams, $timeout, uScope, dmdProvider) {
      var fetchPublication, init;
      fetchPublication = function() {
        return uScope.indicate($scope, dmdProvider.fetch($scope.id), 'fetchingPublication');
      };
      init = function() {
        $scope.id = $routeParams.id;
        return fetchPublication().then(function(data) {
          return $scope.publication = data;
        })["catch"](function(response) {
          return $scope.error = response.status === 404 ? 'Publication not found.' : 'Something went wrong. Sorry.';
        });
      };
      return init();
    }
  ]);

}).call(this);

//# sourceMappingURL=../../app/controllers/publication.js.map