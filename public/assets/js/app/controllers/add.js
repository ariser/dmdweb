(function() {
  'use_strict';
  angular.module('dmdApp').controller('dmdAddPublicationCtrl', [
    '$scope', '$location', 'uScope', 'dmdProvider', function($scope, $location, uScope, dmdProvider) {
      var init;
      $scope.create = function(data) {
        return uScope.indicate($scope, dmdProvider.add(data), 'creatingPublication').then(function(data) {
          return $location.path('/publications/' + data.id);
        });
      };
      init = function() {};
      return init();
    }
  ]);

}).call(this);

//# sourceMappingURL=../../app/controllers/add.js.map