(function() {
  'use_strict';
  angular.module('dmdApp').controller('dmdEditPublicationCtrl', [
    '$scope', '$routeParams', '$timeout', 'uScope', 'dmdProvider', function($scope, $routeParams, $timeout, uScope, dmdProvider) {
      var fetchPublication, init;
      fetchPublication = function() {
        return uScope.indicate($scope, dmdProvider.fetch($scope.id), 'fetchingPublication');
      };
      $scope.edit = function(data) {
        return uScope.indicate($scope, dmdProvider.update($scope.id, data), 'updatingPublication').then(function(data) {
          return $location.path('/publications/' + data.id);
        });
      };
      init = function() {
        $scope.id = $routeParams.id;
        return fetchPublication().then(function(data) {
          $scope.publication = data;
          return $timeout(function() {
            return $scope.$broadcast('dataLoaded');
          });
        })["catch"](function(response) {
          return $scope.error = response.status === 404 ? 'Publication not found.' : 'Something went wrong. Sorry.';
        });
      };
      return init();
    }
  ]);

}).call(this);

//# sourceMappingURL=../../app/controllers/edit.js.map