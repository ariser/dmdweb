(function() {
  'use_strict';
  angular.module('dmdApp').controller('dmdSearchFormCtrl', [
    '$scope', '$location', '$timeout', 'uScope', 'dmdProvider', function($scope, $location, $timeout, uScope, dmdProvider) {
      var init, loadKeywords, loadTypes;
      loadKeywords = function() {
        return uScope.indicate($scope, dmdProvider.fetchKeywords(), 'fetchingKeywords');
      };
      loadTypes = function() {
        return uScope.indicate($scope, dmdProvider.fetchTypes(), 'fetchingTypes');
      };
      $scope.search = function() {
        if ($scope.form.offset) {
          $scope.form.offset = 0;
        }
        return $location.path('/publications').search($scope.form);
      };
      init = function() {
        var query;
        $scope.sortVariants = [
          {
            key: 'title',
            name: 'Title'
          }, {
            key: 'pubType',
            name: 'Paper type'
          }, {
            key: 'rating',
            name: 'Quality'
          }
        ];
        query = angular.copy($location.search());
        if (query.pubType) {
          query.pubType = _(query.pubType).map(Number);
        }
        if (query.start_year) {
          query.start_year = Number(query.start_year);
        }
        if (query.end_year) {
          query.end_year = Number(query.end_year);
        }
        if (query.limit) {
          query.limit = Number(query.limit);
        }
        if (query.offset) {
          query.offset = Number(query.offset);
        }
        $scope.form = _(query).defaults({
          area: null,
          author: null,
          venue: null,
          title: null,
          keyword: null,
          pubType: null,
          institution: null,
          start_year: null,
          end_year: null,
          sort_by: 'title',
          sort_order: 'ASC',
          limit: null,
          offset: null
        });
        $timeout(function() {
          $scope.$broadcast('updateAuthors');
          return $scope.$broadcast('updateInstitutions');
        });
        loadKeywords().then(function(data) {
          $scope.keywords = data;
          return $scope.$broadcast('updateKeywords');
        });
        return loadTypes().then(function(data) {
          $scope.types = data;
          return $scope.$broadcast('updateTypes');
        });
      };
      return init();
    }
  ]);

}).call(this);

//# sourceMappingURL=../../app/controllers/search_form.js.map