(function() {
  'use_strict';
  angular.module('dmdApp').controller('dmdPublicationFormCtrl', [
    '$scope', '$window', 'uScope', 'dmdProvider', function($scope, $window, uScope, dmdProvider) {
      var init, loadTypes;
      loadTypes = function() {
        return uScope.indicate($scope, dmdProvider.fetchTypes(), 'fetchingTypes');
      };
      init = function() {
        var ref;
        $scope.form = _((ref = $scope.form) != null ? ref : {}).defaults({
          title: null,
          authors: [],
          institutions: [],
          pubAbstract: null,
          pubType: null,
          area: null,
          doi: null,
          venue: null,
          volume: null,
          start_page: null,
          end_page: null,
          year: null,
          keywords: []
        });
        if ($scope.form.volume) {
          $scope.form.volume = Number($scope.form.volume);
        }
        if ($scope.form.start_page) {
          $scope.form.start_page = Number($scope.form.start_page);
        }
        if ($scope.form.end_page) {
          $scope.form.end_page = Number($scope.form.end_page);
        }
        if ($scope.form.year) {
          $scope.form.year = Number($scope.form.year);
        }
        loadTypes().then(function(data) {
          var base, ref1;
          $scope.types = data;
          if ((base = $scope.form).pubType == null) {
            base.pubType = (ref1 = $scope.types[0]) != null ? ref1.id : void 0;
          }
          return $scope.$broadcast('updateTypes');
        });
        $scope.$on('dataLoaded', function() {
          $scope.$broadcast('updateAuthors');
          $scope.$broadcast('updateKeywords');
          $scope.$broadcast('updateInstitutions');
          $scope.$broadcast('updateTypes');
          return $scope.publicationForm.$setPristine();
        });
        angular.element($window).on('beforeunload', function(event) {
          if ($scope.publicationForm.$dirty && !$scope.publicationForm.$submitted) {
            return 'You have unsaved changes.';
          }
        });
        return $scope.$on('$locationChangeStart', function(event) {
          if ($scope.publicationForm.$dirty && !$scope.publicationForm.$submitted && !confirm('You have unsaved changes.\n\nAre you sure you want to leave this page?')) {
            return event.preventDefault();
          }
        });
      };
      return init();
    }
  ]);

}).call(this);

//# sourceMappingURL=../../app/controllers/publication_form.js.map