(function() {
  'use_strict';
  angular.module('dmdApp').controller('dmdListCtrl', [
    '$scope', '$location', 'uScope', 'dmdProvider', function($scope, $location, uScope, dmdProvider) {
      var init, search;
      search = function() {
        return uScope.load($scope, dmdProvider.find($location.search()), 'searching');
      };
      $scope.open = function(id) {
        return $location.url('/publications/' + id);
      };
      $scope.indicator = function(id) {
        return $scope['deletingPublication' + id];
      };
      $scope["delete"] = function(id) {
        var publication, publicationIndex;
        publication = _($scope.publications).findWhere({
          id: id
        });
        publicationIndex = _($scope.publications).indexOf(publication);
        if (publicationIndex >= 0) {
          return $scope.publications.splice(publicationIndex, 1);
        }
      };
      init = function() {
        return search();
      };
      return init();
    }
  ]);

}).call(this);

//# sourceMappingURL=../../app/controllers/list.js.map