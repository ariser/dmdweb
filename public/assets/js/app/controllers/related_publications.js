(function() {
  'use_strict';
  angular.module('dmdApp').controller('dmdRelatedPublicationsCtrl', [
    '$scope', '$routeParams', '$location', '$timeout', 'uScope', 'dmdProvider', function($scope, $routeParams, $location, $timeout, uScope, dmdProvider) {
      var fetchPublications, init;
      fetchPublications = function() {
        return uScope.load($scope, dmdProvider.findRelated($scope.id), 'fetchingPublications');
      };
      $scope.open = function(id) {
        return $location.url('/publications/' + id);
      };
      init = function() {
        $scope.id = $routeParams.id;
        return fetchPublications()["catch"](function(response) {
          return $scope.error = response.status === 404 ? 'Publication not found.' : 'Something went wrong. Sorry.';
        });
      };
      return init();
    }
  ]);

}).call(this);

//# sourceMappingURL=../../app/controllers/related_publications.js.map