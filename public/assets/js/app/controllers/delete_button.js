(function() {
  'use_strict';
  angular.module('dmdApp').controller('dmdDeleteButtonCtrl', [
    '$scope', '$location', 'uScope', 'dmdProvider', function($scope, $location, uScope, dmdProvider) {
      return $scope["delete"] = function() {
        if (confirm("Delete this publication? It can't be undone.")) {
          return uScope.indicate($scope.$root, dmdProvider["delete"]($scope.id), 'deletingPublication' + $scope.id).then(function() {
            return typeof $scope.afterDelete === "function" ? $scope.afterDelete() : void 0;
          });
        }
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=../../app/controllers/delete_button.js.map