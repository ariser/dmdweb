(function() {
  'use strict';
  angular.module('flat.ui').directive('fuiCheckbox', function() {
    return {
      restrict: 'A',
      link: function(scope, elem, attrs) {
        return elem.radiocheck();
      }
    };
  });

}).call(this);

//# sourceMappingURL=../flat-ui/checkbox.js.map