(function() {
  'use strict';
  angular.module('flat.ui').directive('fuiSliderSegments', [
    '$timeout', function($timeout) {
      return {
        restrict: 'A',
        replace: false,
        template: '<div class="ui-slider-segment" ng-repeat="segment in segments" style="{{ segment.style }}"></div>',
        link: function(scope, elem, attrs) {
          return $timeout(function() {
            var amount, index, option, orientation;
            option = elem.slider('option');
            amount = (option.max - option.min) / option.step;
            orientation = option.orientation;
            return scope.segments = (function() {
              var i, ref, results;
              results = [];
              for (index = i = 1, ref = amount - (amount % 1 ? 0 : 1); 1 <= ref ? i <= ref : i >= ref; index = 1 <= ref ? ++i : --i) {
                results.push({
                  style: orientation === 'vertical' ? "top: " + (100 / amount * index) + "%" : "margin-left: " + (100 / amount) + "%"
                });
              }
              return results;
            })();
          });
        }
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=../flat-ui/slider.js.map