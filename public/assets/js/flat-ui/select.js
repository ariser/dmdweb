(function() {
  'use strict';
  angular.module('flat.ui').directive('fuiSelect', [
    '$timeout', function($timeout) {
      return {
        restrict: 'A',
        link: function(scope, elem, attrs) {
          var options;
          options = {};
          if (attrs.placeholder != null) {
            options.placeholder = attrs.placeholder;
          }
          elem.select2(options);
          $timeout(function() {
            return elem.select2('val', elem.val());
          });
          return scope.$on(attrs.fuiRefresh, function() {
            return $timeout(function() {
              return elem.select2('val', elem.val());
            });
          });
        }
      };
    }
  ]);

}).call(this);

//# sourceMappingURL=../flat-ui/select.js.map