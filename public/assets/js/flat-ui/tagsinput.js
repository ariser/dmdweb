(function() {
  'use strict';
  angular.module('flat.ui').directive('fuiTagsinput', function() {
    return {
      restrict: 'A',
      link: function(scope, elem, attrs) {
        elem.tagsinput();
        return scope.$on(attrs.fuiRefresh, function() {
          var tags;
          tags = elem.val().split(',');
          elem.tagsinput('removeAll');
          return _(tags).each(function(tag) {
            return elem.tagsinput('add', tag);
          });
        });
      }
    };
  });

}).call(this);

//# sourceMappingURL=../flat-ui/tagsinput.js.map