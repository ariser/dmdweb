(function() {
  'use strict';
  angular.module('utils').directive('xhrIndicator', function() {
    return {
      restrict: 'A',
      template: document.getElementById('/utils/indicator.html').innerHTML,
      transclude: true,
      replace: false,
      scope: {
        indicator: '=xhrIndicator'
      }
    };
  });

}).call(this);

//# sourceMappingURL=../utils/indicator.js.map