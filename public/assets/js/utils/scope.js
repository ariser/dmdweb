(function() {
  'use strict';
  angular.module('utils').factory('uScope', [
    "$q", function($q) {
      var Scope;
      Scope = (function() {
        function Scope() {}

        Scope.prototype.load = function(theScope, promise, indicatorKey) {
          if (indicatorKey == null) {
            indicatorKey = 'loading';
          }
          return this.indicate(theScope, promise, indicatorKey).then(function(data) {
            angular.extend(theScope, data);
            return data;
          })["catch"](function(response) {
            var data, ref;
            if (((ref = response.data) != null ? ref.errors : void 0) == null) {
              data = {
                errors: {
                  "default": ['Something went wrong']
                }
              };
            }
            angular.extend(theScope, data);
            return $q.reject(response);
          });
        };

        Scope.prototype.indicate = function(theScope, promise, indicatorKey) {
          if (indicatorKey == null) {
            indicatorKey = 'loading';
          }
          if (indicatorKey !== false) {
            theScope[indicatorKey] = true;
          }
          return promise.then(function(response) {
            return response.data;
          })["catch"](function(response) {
            return $q.reject(response);
          })["finally"](function() {
            if (indicatorKey !== false) {
              return theScope[indicatorKey] = false;
            }
          });
        };

        return Scope;

      })();
      return new Scope;
    }
  ]);

}).call(this);

//# sourceMappingURL=../utils/scope.js.map