'use strict'

angular.module('utils')

.directive('xhrIndicator', ->
  restrict: 'A'
  template: document.getElementById('/utils/indicator.html').innerHTML
  transclude: true
  replace: false
  scope:
    indicator: '=xhrIndicator'
)