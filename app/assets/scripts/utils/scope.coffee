'use strict'

# this one was taken from MyTime and modified =)

angular.module('utils')

.factory('uScope', [
    "$q"
    ($q) ->
      class Scope
        # this helper function is meant to encapsulate the logic for ajax calls,
        # mostly for loading data but also for actions. given a scope and
        # a promise, the successful result of the promise (expected to be an
        # object with appropriate keys) is put directly on the scope. default
        # failure handling and indicators are also provided.
        #
        # in the future, this could take option to override the default failure
        # handling, or different error message, etc.
        load: (theScope, promise, indicatorKey = 'loading') ->
          @indicate(theScope, promise, indicatorKey)
            .then (data) ->
              angular.extend(theScope, data)
              data
            .catch (response) ->
              # automatically promote errors to the scope
              data = {
                errors:
                  default: ['Something went wrong']
              } unless response.data?.errors?
              angular.extend(theScope, data)
              $q.reject(response)

        indicate: (theScope, promise, indicatorKey = 'loading') ->
          theScope[indicatorKey] = true unless indicatorKey == false
          promise
          .then (response) ->
            response.data
          .catch (response) ->
            $q.reject(response)
          .finally ->
            theScope[indicatorKey] = false unless indicatorKey == false

      new Scope
  ])
