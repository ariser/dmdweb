'use_strict'

angular.module('dmdApp')

.directive('deleteButton', ->
  restrict: 'AE'
  replace: true
  templateUrl: 'delete_button.html'
  controller: 'dmdDeleteButtonCtrl'
  scope:
    id: '='
    afterDelete: '&'
)