'use strict'

angular.module('dmdApp')

.directive('dmdArrayInput', ->
  restrict: 'A'
  require: 'ngModel'
  link: (scope, elem, attrs, ngModel) ->
    ngModel.$formatters.push (value) -> value?.join?(',') ? value
    ngModel.$parsers.push (value) -> if value.toString() then value.toString().split(',') else []
)