'use_strict'

angular.module('dmdApp')

.directive('publicationForm', ->
  restrict: 'AE'
  replace: true
  templateUrl: 'publication_form.html'
  controller: 'dmdPublicationFormCtrl'
  scope:
    submit: '='
    form: '=?'
)