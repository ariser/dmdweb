'use_strict'

angular.module('dmdApp')

.directive('dmdPager', ->
  restrict: 'AE'
  replace: false
  templateUrl: 'pagination.html'
  controller: 'dmdPaginationCtrl'
)