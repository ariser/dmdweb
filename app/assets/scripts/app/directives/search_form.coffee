'use_strict'

angular.module('dmdApp')

.directive('publicationSearch', ->
  restrict: 'AE'
  replace: true
  templateUrl: 'search_form.html'
  controller: 'dmdSearchFormCtrl'
)