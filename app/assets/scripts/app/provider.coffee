'use strict'

angular.module('dmdApp')

.factory('dmdProvider', [
    '$http',
    ($http) ->
      class DMDProvider

        fetchKeywords: ->
          $http.get("/keywords")

        fetchTypes: ->
          $http.get('/paper_types')

        find: (data) ->
          $http.get("/publications", {params: data})

        add: (data) ->
          $http.post("/publications", data)

        fetch: (id) ->
          $http.get("/publications/#{id}")

        update: (id, data) ->
          $http.put("/publications/#{id}", data)

        delete: (id) ->
          $http.delete("/publications/#{id}")

        findRelated: (id) ->
          $http.get("/publications/#{id}/related")

      new DMDProvider
  ])