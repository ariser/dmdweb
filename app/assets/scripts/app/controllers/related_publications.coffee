'use_strict'

angular.module('dmdApp')

.controller('dmdRelatedPublicationsCtrl', [
    '$scope', '$routeParams', '$location', '$timeout', 'uScope', 'dmdProvider',
    ($scope, $routeParams, $location, $timeout, uScope, dmdProvider) ->
      fetchPublications = ->
        uScope.load($scope, dmdProvider.findRelated($scope.id), 'fetchingPublications')

      $scope.open = (id) ->
        $location.url('/publications/' + id)

      init = ->
        $scope.id = $routeParams.id

        fetchPublications().catch (response) ->
          $scope.error = if response.status == 404 then 'Publication not found.' else 'Something went wrong. Sorry.'

      init()
  ])