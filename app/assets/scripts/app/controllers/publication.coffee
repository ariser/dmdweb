'use_strict'

angular.module('dmdApp')

.controller('dmdPublicationCtrl', [
    '$scope', '$routeParams', '$timeout', 'uScope', 'dmdProvider',
    ($scope, $routeParams, $timeout, uScope, dmdProvider) ->
      fetchPublication = ->
        uScope.indicate($scope, dmdProvider.fetch($scope.id), 'fetchingPublication')

      init = ->
        $scope.id = $routeParams.id

        fetchPublication()
        .then (data) ->
          $scope.publication = data
        .catch (response) ->
          $scope.error = if response.status == 404 then 'Publication not found.' else 'Something went wrong. Sorry.'

      init()
  ])