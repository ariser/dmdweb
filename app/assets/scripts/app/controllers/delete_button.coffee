'use_strict'

angular.module('dmdApp')

.controller('dmdDeleteButtonCtrl', [
    '$scope', '$location', 'uScope', 'dmdProvider',
    ($scope, $location, uScope, dmdProvider) ->

      $scope.delete = ->
        if confirm("Delete this publication? It can't be undone.")
          uScope.indicate($scope.$root, dmdProvider.delete($scope.id), 'deletingPublication' + $scope.id)
          .then ->
            $scope.afterDelete?()
  ])