'use_strict'

angular.module('dmdApp')

.controller('dmdPublicationFormCtrl', [
    '$scope', '$window', 'uScope', 'dmdProvider',
    ($scope, $window, uScope, dmdProvider) ->
      loadTypes = ->
        uScope.indicate($scope, dmdProvider.fetchTypes(), 'fetchingTypes')

      init = ->
        $scope.form = _($scope.form ? {}).defaults(
          title: null
          authors: []
          institutions: []
          pubAbstract: null
          pubType: null
          area: null
          doi: null
          venue: null
          volume: null
          start_page: null
          end_page: null
          year: null
          keywords: []
        )

        $scope.form.volume = Number($scope.form.volume) if $scope.form.volume
        $scope.form.start_page = Number($scope.form.start_page) if $scope.form.start_page
        $scope.form.end_page = Number($scope.form.end_page) if $scope.form.end_page
        $scope.form.year = Number($scope.form.year) if $scope.form.year

        loadTypes().then (data) ->
          $scope.types = data
          $scope.form.pubType ?= $scope.types[0]?.id
          $scope.$broadcast('updateTypes')

        $scope.$on 'dataLoaded', ->
          $scope.$broadcast('updateAuthors')
          $scope.$broadcast('updateKeywords')
          $scope.$broadcast('updateInstitutions')
          $scope.$broadcast('updateTypes')
          $scope.publicationForm.$setPristine() # initial data loading sets $dirty to true, which is wrong

        angular.element($window).on('beforeunload', (event) ->
          if $scope.publicationForm.$dirty and
          not $scope.publicationForm.$submitted
            return 'You have unsaved changes.'
        )
        $scope.$on('$locationChangeStart', (event) ->
          if $scope.publicationForm.$dirty and
          not $scope.publicationForm.$submitted and
          not confirm('You have unsaved changes.\n\nAre you sure you want to leave this page?')
            event.preventDefault()
        )

      init()
  ])