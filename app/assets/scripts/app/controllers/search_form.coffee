'use_strict'

angular.module('dmdApp')

.controller('dmdSearchFormCtrl', [
    '$scope', '$location', '$timeout', 'uScope', 'dmdProvider',
    ($scope, $location, $timeout, uScope, dmdProvider) ->
      loadKeywords = ->
        uScope.indicate($scope, dmdProvider.fetchKeywords(), 'fetchingKeywords')

      loadTypes = ->
        uScope.indicate($scope, dmdProvider.fetchTypes(), 'fetchingTypes')

      $scope.search = ->
        $scope.form.offset = 0 if $scope.form.offset
        $location.path('/publications').search($scope.form);

      init = ->
        $scope.sortVariants = [
          {key: 'title', name: 'Title'}
          {key: 'pubType', name: 'Paper type'}
          {key: 'rating', name: 'Quality'}
        ]

        query = angular.copy($location.search())

        query.pubType = _(query.pubType).map(Number) if query.pubType
        query.start_year = Number(query.start_year) if query.start_year
        query.end_year = Number(query.end_year) if query.end_year
        query.limit = Number(query.limit) if query.limit
        query.offset = Number(query.offset) if query.offset

        $scope.form = _(query).defaults(
          area: null
          author: null
          venue: null
          title: null
          keyword: null
          pubType: null
          institution: null
          start_year: null
          end_year: null
          sort_by: 'title'
          sort_order: 'ASC'
          limit: null
          offset: null
        )

        $timeout ->
          $scope.$broadcast('updateAuthors')
          $scope.$broadcast('updateInstitutions')

        loadKeywords().then (data) ->
          $scope.keywords = data
          $scope.$broadcast('updateKeywords')

        loadTypes().then (data) ->
          $scope.types = data
          $scope.$broadcast('updateTypes')

      init()
  ])