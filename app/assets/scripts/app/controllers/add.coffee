'use_strict'

angular.module('dmdApp')

.controller('dmdAddPublicationCtrl', [
    '$scope', '$location', 'uScope', 'dmdProvider',
    ($scope, $location, uScope, dmdProvider) ->
      $scope.create = (data) ->
        uScope.indicate($scope, dmdProvider.add(data), 'creatingPublication')
        .then (data) ->
          $location.path('/publications/' + data.id)

      init = ->


      init()
  ])
