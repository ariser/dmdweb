'use_strict'

angular.module('dmdApp')

.controller('dmdEditPublicationCtrl', [
    '$scope', '$routeParams', '$timeout', 'uScope', 'dmdProvider',
    ($scope, $routeParams, $timeout, uScope, dmdProvider) ->
      fetchPublication = ->
        uScope.indicate($scope, dmdProvider.fetch($scope.id), 'fetchingPublication')

      $scope.edit = (data) ->
        uScope.indicate($scope, dmdProvider.update($scope.id, data), 'updatingPublication')
        .then (data) ->
          $location.path('/publications/' + data.id)

      init = ->
        $scope.id = $routeParams.id

        fetchPublication()
        .then (data) ->
          $scope.publication = data
          $timeout -> $scope.$broadcast('dataLoaded')
        .catch (response) ->
          $scope.error = if response.status == 404 then 'Publication not found.' else 'Something went wrong. Sorry.'

      init()
  ])
