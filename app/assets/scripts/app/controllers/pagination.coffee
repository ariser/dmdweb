'use_strict'

angular.module('dmdApp')

.controller('dmdPaginationCtrl', [
    '$scope', '$location',
    ($scope, $location) ->
      $scope.prev = ->
        $location.search('offset', Math.max($scope.offset - $scope.limit, 0))

      $scope.next = ->
        $location.search('offset', $scope.offset + $scope.limit)
  ])