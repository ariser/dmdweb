'use_strict'

angular.module('dmdApp')

.controller('dmdListCtrl', [
    '$scope', '$location', 'uScope', 'dmdProvider',
    ($scope, $location, uScope, dmdProvider) ->
      search =->
        uScope.load($scope, dmdProvider.find($location.search()), 'searching')

      $scope.open = (id) ->
        $location.url('/publications/' + id)

      $scope.indicator = (id) -> $scope['deletingPublication' + id]

      $scope.delete = (id) ->
        publication = _($scope.publications).findWhere({id})
        publicationIndex = _($scope.publications).indexOf(publication)
        $scope.publications.splice(publicationIndex, 1) if publicationIndex >= 0

      init = ->
        search()

      init()
  ])