'use strict'

angular.module('dmdApp')

.run([
    '$rootScope', '$window',
    ($rootScope, $window) ->
      $rootScope.$back = -> $window.history.back()
  ])

.config([
    '$httpProvider',
    ($httpProvider) ->
      $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest' # ajax requests signature
  ])