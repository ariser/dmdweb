# `flat.ui` module

The `flat.ui` module is helper for Flat UI js-based components. Basically, it allows to initialize them using directives.