'use strict'

angular.module('flat.ui')

.directive('fuiCheckbox', ->
  restrict: 'A'
  link: (scope, elem, attrs) ->
    elem.radiocheck();
)