'use strict'

angular.module('flat.ui')

.directive('fuiTagsinput', ->
  restrict: 'A'
  link: (scope, elem, attrs) ->
    elem.tagsinput();

    # hook for refreshing state on demand. For dynamic inputs.
    scope.$on(attrs.fuiRefresh, ->
      tags = elem.val().split(',')
      elem.tagsinput('removeAll')
      _(tags).each (tag) -> elem.tagsinput('add', tag)
    )
)