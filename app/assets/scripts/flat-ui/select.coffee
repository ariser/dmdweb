'use strict'

angular.module('flat.ui')

.directive('fuiSelect', [
    '$timeout',
    ($timeout) ->
      restrict: 'A'
      link: (scope, elem, attrs) ->
        # TODO implement options
        options = {}
        options.placeholder = attrs.placeholder if attrs.placeholder?
        elem.select2(options);
        $timeout -> elem.select2('val', elem.val())

        # hook for refreshing state on demand. For dynamic selects.
        scope.$on(attrs.fuiRefresh, ->
          $timeout -> elem.select2('val', elem.val())
        )
  ])