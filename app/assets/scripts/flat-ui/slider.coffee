'use strict'

angular.module('flat.ui')

.directive('fuiSliderSegments', [
    '$timeout',
    ($timeout)->
      restrict: 'A'
      replace: false
      template: '<div class="ui-slider-segment" ng-repeat="segment in segments" style="{{ segment.style }}"></div>'
      link: (scope, elem, attrs) ->
        $timeout -> # let slider initialize
          option = elem.slider('option')
          amount = (option.max - option.min) / option.step
          orientation = option.orientation

          scope.segments = (
            {
            style: if orientation == 'vertical' then "top: #{100 / amount * index}%" else "margin-left: #{100 / amount}%"
            } for index in [1..amount - if amount % 1 then 0 else 1]
          )
  ])