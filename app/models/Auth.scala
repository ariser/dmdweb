package models

import play.api.mvc._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

trait AuthenticatedRequest[+A] extends Request[A] {
  val user: Option[AuthUser]
}

object AuthenticatedRequest {
  def apply[A](u: Option[AuthUser], r: Request[A]) = new AuthenticatedRequest[A] {
    def id = r.id

    def tags = r.tags

    def uri = r.uri

    def path = r.path

    def method = r.method

    def version = r.version

    def queryString = r.queryString

    def headers = r.headers

    lazy val remoteAddress = r.remoteAddress

    def username = None

    val body = r.body
    val user = u

    def secure: Boolean = r.secure
  }
}

trait Authenticator {
  def invokeBlock[A](request: Request[A], block: (AuthenticatedRequest[A]) => Future[Result]) = {
    val user = getUser(request)
    if (valid(user))
      block(AuthenticatedRequest[A](user, request))
    else {
      Future {
        Results.Redirect("/login")
      }
    }
  }

  def valid(admin: Option[AuthUser]): Boolean = admin.isDefined

  def getUser(request: RequestHeader) = {
    if (!request.session.isEmpty) {
      val login = request.session.get(Security.username)
      if (login.isDefined) Option(AuthUser(login.get))
      else Option.empty
    }
    else Option.empty
  }
}


trait AuthenticatorAjax {
  def invokeBlock[A](request: Request[A], block: (AuthenticatedRequest[A]) => Future[Result]) = {
    val user = getUser(request)
    if (valid(user))
      block(AuthenticatedRequest[A](user, request))
    else {
      Future {
        Results.Unauthorized
      }
    }
  }

  def valid(admin: Option[AuthUser]): Boolean = admin.isDefined

  def getUser(request: RequestHeader) = {
    if (!request.session.isEmpty) {
      val login = request.session.get(Security.username)
      if (login.isDefined) Option(AuthUser(login.get))
      else Option.empty
    }
    else Option.empty
  }
}

object AuthenticatedWithRedirect extends ActionBuilder[AuthenticatedRequest] with Authenticator

object Authenticated extends ActionBuilder[AuthenticatedRequest] with AuthenticatorAjax