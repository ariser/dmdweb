package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.libs.json.Json
import play.api.Play.current

object Keyword {
  val keyword = {
    get[String]("name")
  }

  def add(keyword: String) = {
    DB.withConnection { implicit c =>
      val q = SQL(s"INSERT INTO keyword (name) VALUES ('${keyword.replaceAll("'", "''")}');")
      q.executeInsert()
    }
  }

  def addList(keywords: Array[String]) = {
    keywords foreach add
  }

  def all: List[String] = {
    DB.withConnection { implicit c =>
      SQL("SELECT name FROM keyword;").as(keyword *)
    }
  }
}
