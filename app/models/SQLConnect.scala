package models

import java.io.PrintStream
import java.net.{InetAddress, Socket}

import scala.io.BufferedSource

object SQLConnect {
  val defPort = 4551

  val defHostName = "localhost"

  def query(action: String) = {
    val socket = new Socket(InetAddress.getByName(defHostName), defPort)
    lazy val in = new BufferedSource(socket.getInputStream).getLines()
    val out = new PrintStream(socket.getOutputStream)

    out.println("Hello, world")
    out.flush()
    val r = "Received: " + in.next()

    socket.close()
  }
}