package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.libs.json.Json
import play.api.Play.current

case class PaperType(id: Long, paperType: String, title: String)

object PaperType {
  val paperType = {
    get[Long]("id") ~ get[String]("title") map {
      case id ~ title => PaperType(id, null, title)
    }
  }

  def all: List[PaperType] = {
    DB.withConnection { implicit c =>
      SQL("SELECT id, title FROM types;").as(paperType *)
    }
  }

  implicit val paperTypeFormat = Json.format[PaperType]
}
