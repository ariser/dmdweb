package models

import anorm._
import anorm.SqlParser._
import play.api.db.DB
import play.api.Play.current

case class AuthUser(login: String)

object AuthUser {
  val userLogin = {
    get[String]("login")
  }

  def authenticate(login: String, password: String): Boolean = {
    DB.withConnection { implicit c =>
      SQL("SELECT login FROM users WHERE login = {login} AND password = {password}").on(
        'login -> login,
        'password -> password
      ).as(userLogin *)
    }.nonEmpty
  }

  def register(login: String, password: String) = {
    DB.withConnection { implicit c =>
      SQL("INSERT INTO users (login, password) VALUES ({login}, {password});")
        .on(
          'login -> login,
          'password -> password
        ).executeInsert()
    }
  }
}