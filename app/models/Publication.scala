package models

import play.api.db.DB
import anorm._
import anorm.SqlParser._
import play.api.Play.current
import play.api.libs.json.Json

case class Publication(id: Long,
                       title: String,
                       authors: List[String],
                       institutions: List[String],
                       pubAbstract: String,
                       pubType: Long,
                       pubTypeTitle: String,
                       area: Option[String],
                       doi: String,
                       venue: String,               // If it is a book, venue and title are the same
                       volume: Option[Int],        // If it is a book or proceeding we return null
                       start_page: Option[Int],
                       end_page: Option[Int],
                       year: Int,
                       keywords: List[String],
                       rating: Option[Short])

case class NewPublication(title: String,
                          authors: List[String],
                          institutions: List[String],
                          pubAbstract: String,
                          pubType: Long,
                          area: Option[String],
                          doi: String,
                          venue: String,
                          volume: Option[Int],
                          start_page: Option[Int],
                          end_page: Option[Int],
                          year: Int,
                          keywords: List[String])

case class PublicationsPages(publications: List[Publication],
                             limit: Short,
                             offset: Long,
                             hasPrev: Boolean,
                             hasNext: Boolean)

case class SearchQuery(area: Option[String],
                       author: Option[List[String]],
                       venue: Option[String],
                       title: Option[String],
                       keyword: Option[List[String]],
                       pubType: Option[List[Long]],
                       institution: Option[List[String]],

                       start_year: Option[Int] = Some(1), // TODO default value doesn't work when you parse json (maybe at all)
                       end_year: Option[Int] = Some(9999), // TODO default value doesn't work when you parse json (maybe at all)

                       sort_by: Option[String] = Some("title"), // TODO default value doesn't work when you parse json (maybe at all)
                       sort_order: Option[String] = Some("ASC"), // TODO default value doesn't work when you parse json (maybe at all)

                       limit: Option[Short] = Some(50), // TODO default value doesn't work when you parse json (maybe at all)
                       offset: Option[Long] = Some(0) // TODO default value doesn't work when you parse json (maybe at all)
                        )

object Publication {
  val publication = {
    get[Long]("id")~
    get[String]("title")~
    get[String]("authors_string")~
    get[Option[String]]("institutions_string")~
    get[String]("abstract")~
    get[Long]("type_id")~
    get[String]("type_title")~
    get[Option[String]]("area")~                    // By now it is option
    get[String]("doi")~
    get[String]("venue")~
    get[Option[Int]]("volume")~
    get[Option[Int]]("start_page")~
    get[Option[Int]]("end_page")~
    get[Int]("year")~
    get[Option[String]]("keywords_string")~
    get[Option[Short]]("rating") map {
      case id~title~authors_string~institutions_string~
           pubAbstract~pubType~pubTypeTitle~area~doi~venue~volume~
           start_page~end_page~year~keywords_string~rating =>
        Publication(id, title, authors_string.split(", ").toList,
          institutions_string.getOrElse("").split(", ").toList,
          pubAbstract, pubType, pubTypeTitle, area, doi, venue,
          volume, start_page, end_page, year,
          keywords_string.getOrElse("").split(", ").toList, rating)
    }
  }

  val publication_short = {
    get[Long]("id")~
    get[String]("title")~
    get[String]("authors_string")~
    get[String]("type_title") map {
      case id~title~authors_string~pubTypeTitle =>
        Publication(id, title, authors_string.split(", ").toList,
          "".split(", ").toList,
          "", -1, pubTypeTitle, None, "", "", None, None, None, -1,
          "".split(", ").toList, null)
    }
  }

  def create(publication: NewPublication): Unit = {
    DB.withConnection { implicit c =>
      val pubType = publication.pubType
      val venue = publication.venue.replaceAll("'", "''") // Sanitize lol
      val year = publication.year
      val pubData =
        Array(publication.pubAbstract.replaceAll("'", "''"),
              publication.doi.replaceAll("'", "''"),
              publication.title.replaceAll("'", "''")).mkString("', '")

      if (SQL(s"SELECT id FROM publication WHERE doi = '${publication.doi}';")().nonEmpty)
        throw new IllegalStateException("Publication with the same doi is already in DB!")

      val pubId: Option[Long] = SQL(s"""
                          INSERT INTO publication (abstract, doi, title, type, rating)
                          VALUES ('$pubData', $pubType, 10);
                          """).executeInsert()

      if (pubId.isEmpty)
        throw new IllegalStateException("Could not add publication into the relation! ! ! ")


      // Let's add magazine or conference (or book) if it's not present id DB
      val venueId: Option[Long] = pubType match {
        case 1 => val magazine_info = SQL(s"""
                                      SELECT id,
                                      EXTRACT (year FROM history_start) as year_start,
                                      EXTRACT (year FROM history_end) as year_end
                                      FROM magazine WHERE title = '$venue';""")
          if (magazine_info().isEmpty)
            SQL(
              s"""
               INSERT INTO magazine (title, history_start, history_end)
               VALUES ('$venue', date'$year-01-01', date'$year-12-31')
               """).executeInsert()
          else {
            // Let's correct history of journal if it's needed
            if (magazine_info.as(SqlParser.int("year_start").single) > year)
              SQL(s"UPDATE magazine SET history_start = date'$year-01-01' WHERE title = '$venue'")
            if (magazine_info.as(SqlParser.int("year_end").single) < year)
              SQL(s"UPDATE magazine SET history_end = date'$year-01-01' WHERE title = '$venue'")

            Some(magazine_info.as(SqlParser.long("id").single))
          }

        case 2 => val conference_info = SQL(s"""
                                        SELECT id, date FROM conference
                                        WHERE title = '$venue' AND date = date'$year-01-01' """)
          if (conference_info().isEmpty)
            SQL(
              s"""
              INSERT INTO conference (title, date)
              VALUES ('$venue', date'$year-01-01')
              """).executeInsert()
          else
            Some(conference_info.as(SqlParser.long("id").single))

        case 3 => SQL(s"INSERT INTO book (publication_id) VALUES $pubId;").executeInsert()

      }

      if (venueId.isEmpty)
        throw new IllegalStateException("Could not obtain venue id ! ! ! ")

      // Let's create appropriate tuple in 'contains' table
      pubType match {
        case 1 =>
                // We can trace error if in DB exists tuple with some attribute -1
            val volume = publication.volume.getOrElse(-1)
            val start_page = publication.start_page.getOrElse(-1)
            val end_page = publication.end_page.getOrElse(-1)
            val year = publication.year
            SQL(s"""
            INSERT INTO magazine_contains (magazine_id, publication_id, volume, start_page, end_page, year)
            VALUES (${venueId.get}, ${pubId.get}, $volume, $start_page, $end_page, date'$year-01-01');
            """).executeInsert()
        case 2 =>
            // We can trace error if in DB exists tuple with some attribute -1
            val start_page = publication.start_page.getOrElse(-1)
            val end_page = publication.end_page
            val year = publication.year
            SQL(s"""
            INSERT INTO conference_contains (magazine_id, publication_id, start_page, end_page, year)
            VALUES (${venueId.get}, ${pubId.get}, $start_page, $end_page, date'$year-01-01');
            """).executeInsert()
          // In case of book we don't need to do something
      }


      // Inserting new authors    // We throw exception if pubId isn't defined
      val authorIds = updateAuthorsInfo(publication.authors, pubId.getOrElse(-1))

      // TODO Institutions should relate to an author, by now we add all-to-all tuples in 'affiliates'
      updateInstitutionsInfo(publication.institutions, authorIds, year)

      updateKeywordsInfo(publication.keywords, pubId.getOrElse(-1))
    }
  }

  def update(publication: Publication): Unit = {
    DB.withConnection { implicit  c =>
      val oldPubType =
        SQL(s"SELECT type FROM publication WHERE id = ${publication.id}").as(SqlParser.int("type").singleOpt)

      if(oldPubType.isEmpty)
        throw new IllegalArgumentException("You try to update non-existing publication!")

      if (oldPubType.get != publication.pubType)
        throw new IllegalArgumentException("You try to change publication type!")

      SQL(
        s"""
           UPDATE publication SET
           title = '${publication.title}'
           abstract = '${publication.pubAbstract}',
           doi = '${publication.doi}',
           type = ${publication.pubType},
           rating = ${publication.rating}
           WHERE id = ${publication.id};
        """)

      val pubType = publication.pubType
      val venue = publication.venue
      val year = publication.year
      val pubId = publication.id

      val venueId = updateVenueInfo(pubId, pubType, venue, year).getOrElse(-1)

      pubType match {
        case 1 =>
          // We can trace error if in DB exists tuple with some attribute -1
          val volume = publication.volume.getOrElse(-1)
          val start_page = publication.start_page.getOrElse(-1)
          val end_page = publication.end_page.getOrElse(-1)
          val year = publication.year
          SQL(s"""
            INSERT INTO magazine_contains (magazine_id, publication_id, volume, start_page, end_page, year)
            VALUES ($venueId, $pubId, $volume, $start_page, $end_page, date'$year-01-01');
            """).executeInsert()
        case 2 =>
          // We can trace error if in DB exists tuple with some attribute -1
          val start_page = publication.start_page.getOrElse(-1)
          val end_page = publication.end_page
          val year = publication.year
          SQL(s"""
            INSERT INTO conference_contains (magazine_id, publication_id, start_page, end_page, year)
            VALUES ($venueId, $pubId, $start_page, $end_page, date'$year-01-01');
            """).executeInsert()
        // In case of book we don't need to do something
      }

      // Inserting new authors
      val authorIds = updateAuthorsInfo(publication.authors, pubId)

      // TODO Institutions should relate to an author, by now we add all-to-all tuples in 'affiliates'
      updateInstitutionsInfo(publication.institutions, authorIds, year)

      updateKeywordsInfo(publication.keywords, pubId)

    }
  }

  def updateVenueInfo(pubId: Long, pubType: Long, venue: String, year: Int): Option[Long] = {
    DB.withConnection { implicit c =>
      pubType match {
        case 1 => val magazine_info = SQL(
          s"""
                                      SELECT id,
                                      EXTRACT (year FROM history_start) as year_start,
                                      EXTRACT (year FROM history_end) as year_end
                                      FROM magazine WHERE title = '$venue';""")
          if (magazine_info().isEmpty)
            SQL(
              s"""
               INSERT INTO magazine (title, history_start, history_end)
               VALUES ('$venue', date'$year-01-01', date'$year-12-31')
               """).executeInsert()
          else
          // Let's correct history of journal if it's needed
          if (magazine_info.as(SqlParser.int("year_start").single) > year)
            SQL(s"UPDATE magazine SET history_start = date'$year-01-01' WHERE title = '$venue'")
          if (magazine_info.as(SqlParser.int("year_end").single) < year)
            SQL(s"UPDATE magazine SET history_end = date'$year-01-01' WHERE title = '$venue'")

          Some(magazine_info.as(SqlParser.long("id").single))

        case 2 => val conference_info = SQL(
          s"""
                                        SELECT id, date FROM conference
                                        WHERE title = '$venue' AND date = date'$year-01-01' """)
          if (conference_info().isEmpty)
            SQL(
              s"""
              INSERT INTO conference (title, date)
              VALUES ('$venue', date'$year-01-01')
              """).executeInsert()
          else
            Some(conference_info.as(SqlParser.long("id").single))

        case 3 => SQL(s"INSERT INTO book (publication_id) VALUES $pubId;").executeInsert()
      }
    }
  }

  def updateAuthorsInfo(authors: List[String], pubId: Long): List[Long] = {
    DB.withConnection { implicit c =>
      val authorIds: List[Option[Long]] = authors.map(author =>
        if (SQL(s"SELECT * FROM author WHERE name = '${author.replaceAll("'", "''")}';")().isEmpty)
          SQL(s"INSERT INTO author (name) VALUES ('${author.replaceAll("'", "''")}');").executeInsert()
        else
          Some(SQL(s"SELECT id FROM author WHERE name = " +
            s"'${author.replaceAll("'", "''")}'").as(SqlParser.long("id").single))
      )

      // Inserting new author pairs
      authorIds.combinations(2).toList.map (pair =>
        List(pair.head.getOrElse(-1), pair(1).getOrElse(-1))).map(
        pair => SQL(s"INSERT INTO works_with (author_1_id, author_2_id) " +
          s"VALUES (${pair.mkString(", ")});").executeInsert()
      )


      // Insert 'writes' relationship tuples
      authorIds.map(authorId =>
        SQL(s"INSERT INTO writes (author_id, publication_id) VALUES (${authorId.get}, $pubId);").executeInsert()
      )

      authorIds.map(_.getOrElse(-1L))
    }
  }

  def updateInstitutionsInfo(institutions: List[String], authorIds: List[Long], year: Int): List[Long] = {
    DB.withConnection { implicit c =>
      val institutionIds: List[Option[Long]] = institutions.map(institution =>
        if (SQL(s"SELECT * FROM institution WHERE name = '${institution.replaceAll("'", "'")}';")().isEmpty)
          SQL(s"INSERT INTO institution (name) VALUES ('${institution.replaceAll("'", "'")}');").executeInsert()
        else
          Some(SQL(s"SELECT id FROM institution WHERE name = " +
            s"'${institution.replaceAll("'", "'")}'").as(SqlParser.long("id").single))
      )

      (for {a <- authorIds; i <- institutionIds} yield List(a, i.getOrElse(-1))).map(pair =>
        if (SQL(s"SELECT * FROM affiliates " +
          s"WHERE author_id = ${pair.head} AND " +
          s"institution_id = ${pair(1)}")().nonEmpty)
          SQL(
            s"""
              UPDATE affiliates SET
              start_year LEAST(start_year, date'$year-01-01'),
              end_date GREATEST(end_year, date'$year-12-31');
              """)
        else
          SQL(
            s"""
          INSERT INTO affiliates (author_id, institution_id, start_date, end_date) VALUES
          (${pair.mkString(", ")}, date'$year-01-01', date'$year-12-31');
            """).executeInsert()
      )

      institutionIds.map(_.getOrElse(-1L))
    }
  }

  def updateKeywordsInfo(keywords: List[String], pubId: Long): List[Long] = {
    DB.withConnection { implicit c =>
      val keywordIds: List[Option[Long]] = keywords.map(keyword =>
        if (SQL(s"SELECT * FROM keyword WHERE name = '${keyword.replaceAll("'", "''")}';")().isEmpty)
          SQL(s"INSERT INTO keyword (name) VALUES ('${keyword.replaceAll("'", "''")}');").executeInsert()
        else
          Some(SQL(s"SELECT id FROM keyword WHERE name = " +
            s"'${keyword.replaceAll("'", "''")}'").as(SqlParser.long("id").single))
      )

      keywordIds.map(keywordId =>
        SQL(s"INSERT INTO relates (publication_id, keyword_id) " +
          s"VALUES ($pubId, ${keywordId.getOrElse(-1)});").executeInsert()
      )

      keywordIds.map(_.getOrElse(-1L))
    }
  }

  def delete(id: Long): Unit = {
    DB.withConnection { implicit c =>
      SQL(s"DELETE FROM publication WHERE id = $id").executeUpdate()
    }
  }

  def exists(id: Long): Boolean = {
    DB.withConnection { implicit c =>
      SQL(s"SELECT id FROM publication WHERE id = $id").apply().nonEmpty
    }
  }

  // TODO work with books is very bad
  def getById(id: Long): Option[Publication] = {
    DB.withConnection { implicit  c =>
      SQL(s"""
        WITH pub_with_authors AS (
          WITH pub_meta AS (
            WITH pub AS (
              SELECT * FROM publication WHERE id = $id
            )
            SELECT
              (CASE WHEN (magazine.title, conference.title) IS NULL THEN pub.title
              ELSE concat (magazine.title, conference.title)
              END) as venue,
            pub.*, mc.year, mc.volume,
            CONCAT(mc.start_page, cc.start_page) AS start_page,
            CONCAT(mc.end_page, cc.end_page) AS end_page

            FROM pub
            LEFT OUTER JOIN magazine_contains AS mc ON mc.publication_id = pub.id
            LEFT OUTER JOIN magazine ON mc.magazine_id = magazine.id
            LEFT OUTER JOIN conference_contains as cc ON cc.publication_id = pub.id
            LEFT OUTER JOIN conference ON cc.conference_id = conference.id
          )
          SELECT pub_meta.id, pub_meta.title, pub_meta.abstract, pub_meta.doi, pub_meta.venue,
                 pub_meta.volume, EXTRACT (YEAR FROM pub_meta.year)::INTEGER AS year,
                 pub_meta.start_page::INTEGER, pub_meta.end_page::INTEGER, pub_meta.rating::SMALLINT,
                 types.title AS type_title, types.id AS type_id,
          STRING_AGG(author.name, ', ') AS authors_string,
          STRING_AGG(institution.name, ', ') AS institutions_string

          FROM pub_meta

          JOIN types ON pub_meta.type = types.id

          LEFT OUTER JOIN writes ON pub_meta.id = writes.publication_id
          LEFT OUTER JOIN author ON author.id = writes.author_id

          LEFT OUTER JOIN affiliates ON author.id = affiliates.author_id AND
          pub_meta.year BETWEEN affiliates.start_date AND affiliates.end_date
          LEFT OUTER JOIN institution ON institution.id = affiliates.institution_id

          GROUP BY pub_meta.id, pub_meta.title, pub_meta.abstract, pub_meta.doi, pub_meta.venue,
          pub_meta.volume, year, pub_meta.start_page,
          pub_meta.end_page, pub_meta.rating, types.title, types.id
        )
        SELECT pwa.*, STRING_AGG(keyword.name, ', ') AS keywords_string, '' AS area

        FROM pub_with_authors AS pwa

        LEFT OUTER JOIN relates ON pwa.id = relates.publication_id
        JOIN keyword ON relates.keyword_id = keyword.id

        GROUP BY pwa.id, pwa.title, pwa.abstract, pwa.doi, pwa.type_id,
                 pwa.type_title, pwa.volume, pwa.year, pwa.start_page, pwa.end_page,
                 pwa.authors_string, pwa.institutions_string, pwa.venue, pwa.rating;
      """
      ).as(publication *).headOption
    }
  }


  def find(searchParams: SearchQuery): List[Publication] = {
    /*
     * Create auxiliary temporary view with matching patterns: split by space to array and surround words with %s
     */
    val toSearchAuthor = "CREATE OR REPLACE VIEW  authors_filtered AS (WITH to_search AS (SELECT '%" +
      searchParams.author.getOrElse(List()).map(_.replaceAll("'", "''")).        // Sanitize
        mkString("%' AS pattern UNION ALL SELECT '%") +
      "%' " +
      (if (searchParams.author.getOrElse(List()).length < 2) "AS pattern UNION ALL SELECT '') " else ") " ) +
      "SELECT * FROM author JOIN to_search ON LOWER (author.name) SIMILAR TO to_search.pattern ); "

    val toSearchTitle = "CREATE OR REPLACE VIEW publications_filtered AS (WITH to_search AS (SELECT '%" +
      searchParams.title.getOrElse("").split(" ").map(_.replaceAll("'", "''")).        // Sanitize
        mkString("%' AS pattern UNION ALL SELECT '%") +
      "%' " +
      (if (searchParams.title.getOrElse("").split(" ").length < 2) "AS pattern UNION ALL SELECT '') " else ") " ) +
      "SELECT * FROM publication JOIN to_search ON LOWER (publication.title) SIMILAR TO to_search.pattern ); "


    // TODO don't join on this two tables if you don't really need it!
    val toSearchMagazine = "CREATE OR REPLACE VIEW magazines_filtered AS (WITH to_search AS ( SELECT '%" +
      searchParams.venue.getOrElse("").split(" ").map(_.replaceAll("'", "''")).        // Sanitize
        mkString("%' AS pattern UNION ALL SELECT '%") +
      "%' " +
      (if (searchParams.venue.getOrElse("").split(" ").length < 2) "AS pattern UNION ALL SELECT '') " else ") " ) +
      "SELECT * FROM magazine " +
      "JOIN to_search ON LOWER (magazine.title) SIMILAR TO to_search.pattern " +
      "JOIN magazine_contains ON magazine.id = magazine_contains.magazine_id ); "

    val toSearchConference = "CREATE OR REPLACE VIEW conferences_filtered AS (WITH to_search AS ( SELECT '%" +
      searchParams.venue.getOrElse("").split(" ").map(_.replaceAll("'", "''")).        // Sanitize
        mkString("%' AS pattern UNION ALL SELECT '%") +
      "%' " +
      (if (searchParams.venue.getOrElse("").split(" ").length < 2) "AS pattern UNION ALL SELECT '') " else ") " ) +
      "SELECT * FROM conference " +
      "JOIN to_search ON LOWER (conference.title) SIMILAR TO to_search.pattern " +
      "JOIN conference_contains ON conference.id = conference_contains.conference_id ); "

    val toSearchInstitution = "CREATE OR REPLACE VIEW institutions_filtered AS (WITH to_search AS (SELECT '%" +
      searchParams.institution.getOrElse(List()).map(_.replaceAll("'", "''")).        // Sanitize
        mkString("%' AS pattern UNION ALL SELECT '%") +
      "%' " +
      (if (searchParams.institution.getOrElse(List()).length < 2) "AS pattern UNION ALL SELECT '') " else ") " ) +
      "SELECT * FROM institution JOIN to_search ON LOWER (institution.name) SIMILAR TO to_search.pattern ); "


    // If institution is defined in search query we should replace LEFT OUTER JOIN with JOIN, so:
    var joinInstitutionString: String =
      "LEFT OUTER JOIN institutions_filtered ON institutions_filtered.id = affiliates.institution_id"
    if (searchParams.institution.isDefined)
      joinInstitutionString = "JOIN institutions_filtered ON institutions_filtered.id = affiliates.institution_id"

    val toSearchKeywords = if (searchParams.keyword.isDefined)
      "CREATE OR REPLACE VIEW keywords_filtered AS (WITH to_search AS (SELECT '%" +
      searchParams.keyword.getOrElse(List()).map(_.replaceAll("'", "''")).        // Sanitize
        mkString("%' AS pattern UNION ALL SELECT '%") +
      "%' " +
      (if (searchParams.keyword.getOrElse(List()).length < 2) "AS pattern UNION ALL SELECT '') " else ") " ) +
      "SELECT * FROM keyword JOIN to_search ON LOWER (keyword.name) SIMILAR TO to_search.pattern ); "
      else ""

    // TODO optimize query, order of joins is needed to be corrected
    val joinKeywordsString = if (searchParams.keyword.isDefined)
      "JOIN relates ON pub_with_author.id = relates.publication_id " +
      "JOIN keywords_filtered ON keywords_filtered.id = relates.keyword_id "
    else ""


    val start_year = searchParams.start_year.getOrElse(1)
    val end_year = searchParams.end_year.getOrElse(9999)

    // TODO search by any pubType, in case of none is passed, should be handled in some other way
    val pubType = searchParams.pubType.getOrElse(List(1, 2, 3)).mkString(",")

    val limit = searchParams.limit.getOrElse(10)
    val offset = searchParams.offset.getOrElse(0)

    /*
     * Use 'em! ! !
     */
    DB.withConnection { implicit  c =>
      SQL(toSearchTitle).execute()
      SQL(toSearchAuthor).execute()
      SQL(toSearchMagazine).execute()
      SQL(toSearchConference).execute()
      SQL(toSearchInstitution).execute()

      // This table is very big if you don't look for some specific keywords, so let's don't create it without real need
      if (searchParams.keyword.isDefined)
        SQL(toSearchKeywords).execute()

      val result = SQL(s"""
      WITH pub_with_author AS (
        WITH pub_meta AS (
          WITH pub AS (
            SELECT DISTINCT id, title, type FROM publications_filtered
          )
          SELECT pub.id, pub.title, mc.year, types.title AS type_title FROM pub
          JOIN types ON types.id = pub.type AND pub.type IN ($pubType)
          LEFT OUTER JOIN magazines_filtered as mc ON mc.publication_id = pub.id
          LEFT OUTER JOIN conferences_filtered as cc ON cc.publication_id = pub.id
        )
        SELECT pub_meta.id, pub_meta.title, pub_meta.type_title,
        STRING_AGG(DISTINCT(authors_filtered.name), ', ') AS authors_string,
        STRING_AGG(DISTINCT(institutions_filtered.name), ', ') AS institutions_string

        FROM pub_meta

        JOIN writes ON pub_meta.id = writes.publication_id
        JOIN authors_filtered ON authors_filtered.id = writes.author_id

        LEFT OUTER JOIN affiliates ON authors_filtered.id = affiliates.author_id AND
          pub_meta.year BETWEEN affiliates.start_date AND affiliates.end_date
        $joinInstitutionString

        WHERE pub_meta.year >= date'$start_year-01-01' AND pub_meta.year <= date'$end_year-12-31'

        GROUP BY pub_meta.id, pub_meta.title, pub_meta.type_title
      )

      SELECT pub_with_author.*

        FROM pub_with_author

        $joinKeywordsString

        GROUP BY pub_with_author.id, pub_with_author.title,
        pub_with_author.type_title, pub_with_author.authors_string,
        pub_with_author.institutions_string

      LIMIT $limit
      OFFSET $offset;
      """
      ).as(publication_short *)
      SQL(s"DROP VIEW publications_filtered;").execute()
      SQL(s"DROP VIEW authors_filtered;").execute()
      SQL(s"DROP VIEW magazines_filtered").execute()
      SQL(s"DROP VIEW conferences_filtered").execute()
      SQL(s"DROP VIEW institutions_filtered").execute()
      result
    }
  }

  /*
   * By now this method find related publications based on keywords and pages in journal
   */
  def findRelated(id: Long): List[Publication] = {
    val publication = getById(id).get
    if (publication.pubType == 3) return List()       // Just for now
    /*
    val venueType = if (publication.pubType == 1) "magazine" else "conference"
    val start_page = publication.start_page.getOrElse(-1)
    val end_page = publication.end_page.getOrElse(-1)
    val venue = publication.venue
      WITH by_pages AS (
            SELECT * FROM ${venueType}_contains JOIN $venueType ON
            $venueType.id = ${venueType}_contains.${venueType}_id
            WHERE $venueType.title = $venue AND
              $start_page - start_page < 6 AND end_page - $end_page < 6
          )
     */

    DB.withConnection { implicit c =>
      SQL(
        s"""
          WITH ids AS (
            WITH pub_kw AS (
              SELECT keyword_id AS id FROM publication
              JOIN relates ON publication.id = relates.publication_id
              WHERE publication.id = $id
            )

            SELECT relates.publication_id AS id, COUNT(keyword_id) AS cnt

            FROM
            pub_kw JOIN relates ON pub_kw.id = relates.keyword_id
            GROUP BY publication_id
            ORDER BY cnt DESC
          )

          SELECT publication.id, publication.title,
          types.title AS type_title,
          STRING_AGG (author.name, ', ') AS authors_string

          FROM ids
          JOIN publication ON ids.id = publication.id
          JOIN types ON types.id = publication.type

          JOIN writes ON ids.id = writes.publication_id
          JOIN author ON author.id = writes.author_id

          GROUP BY publication.id, publication.title, types.title;

      """).as(publication_short *)
    }
  }

  implicit val publicationFormat = Json.format[Publication]

}

object NewPublication {
  implicit val newPublicationFormat = Json.format[NewPublication]
}

object PublicationsPages {
  implicit val publicationPagesFormat = Json.format[PublicationsPages]
}

object SearchQuery {
  implicit val searchQueryFormat = Json.format[SearchQuery]
}