
import models.{Keyword, AuthUser}
import play.api._
import play.api.db.DB
import anorm._
import play.api.Play.current

object Global extends GlobalSettings {

  override def onStart(app: Application) {
    StartData.insert()
  }
}

/**
 * Test data initialization
 */
object StartData {

  def insert(): Unit = {
    if (DB.withConnection{implicit c => SQL("SELECT * FROM users;").apply().isEmpty }) {
      AuthUser.register("admin", "admin")
    }

    if (DB.withConnection{implicit c => SQL("SELECT * FROM keyword;").apply().isEmpty }) {
      Keyword.addList("""
        adaptable centres charged collecting compared defect diameter emission etched excitation
        fabrication geometrical immersion integrated lenses observed optically photons planar
        reduced reduction refraction saturated scalable surface
                              """.trim().split("\\s+"))
    }
  }
}