package controllers

import models._
import play.api.mvc._
import play.api.libs.json._

class Publications extends Controller {
  def isAjax(request: Request[Any]): Boolean = {
    val headerOption = request.headers.get("X-Requested-With")
    headerOption.isDefined && headerOption.get.equals("XMLHttpRequest")
  }

  def create = AuthenticatedWithRedirect { implicit request =>
    try {
      val publicationData = request.body.asJson.get.validate[NewPublication].get
      Publication.create(publicationData)
      Ok
    } catch {
      case e: Exception =>
        println(e.printStackTrace())
        BadRequest
    }

    // TODO do we need it?
    /*publicationForm.bindFromRequest.fold(
      errors => BadRequest(views.html.index(Publication.hot(), errors)),
      x => x match { case(title, pubAbstract, doi, authors) => {
        val authorsMap: Map[String, Array[String]] = Map(authors -> new Array[String](0));
        val keywords: Array[String] = Array("abstraction")
        Publication.create(title, authorsMap, pubAbstract, 0, doi, keywords)
        Redirect(routes.Application.publications)
      }
      }
    )*/
  }

  def getSingle(id: Long) = Authenticated { implicit request =>
    if (isAjax(request)) {
      val publicationOption = Publication.getById(id)
      if (publicationOption.isDefined) {
        println(Json.toJson(publicationOption.get)(publicationFull)) // For debug
        Ok(Json.toJson(publicationOption.get)(publicationFull))
      } else {
        NotFound
      }
    } else {
      Ok(views.html.index())
    }
  }

  def delete(id: Long) = Authenticated { implicit request =>
    if (Publication.exists(id)) {
      Publication.delete(id)
      Ok
    } else {
      NotFound
    }
  }

  def update(id: Long) = Authenticated { implicit request =>
    if (Publication.exists(id)) {
      try {
        val publicationData = request.body.asJson.get.validate[Publication].get
        Publication.update(publicationData)
        // TODO return updated publication
        Ok
      } catch {
        case e: Exception => BadRequest(e.getMessage)
      }
    } else {
      NotFound
    }
  }

  def search = Authenticated { implicit request =>
    if (isAjax(request)) {
      try {
        val queryJson = Json.toJson(request.queryString)(searchQueryParser)
        val searchQuery = queryJson.validate[SearchQuery].get
        val publications = Publication.find(searchQuery)
        val result = new PublicationsPages(
          publications,
          searchQuery.limit.getOrElse(50),
          searchQuery.offset.getOrElse(0L),
          searchQuery.offset.getOrElse(0L) > 0,
          searchQuery.limit.getOrElse(50) == publications.size // bad idea but who cares, really
        )
        val resultJson = Json.toJson(result)
        println(resultJson) // For debug

        Ok(resultJson)
      } catch {
        case e: Exception => BadRequest
      }
    } else {
      Ok(views.html.index())
    }
  }

  def findRelated(id: Long) = Authenticated { implicit request =>
    if (isAjax(request)) {
      if (Publication.exists(id)) {
        val publications = Publication.findRelated(id)
        val result = new PublicationsPages(
          publications, 50, 0, false, publications.length == 50
        )
        val resultJson = Json.toJson(result)
        println(resultJson) // For debug

        Ok(resultJson)
      } else {
        NotFound
      }
    } else {
      Ok(views.html.index())
    }
  }

  def keywords = Authenticated { implicit request =>
    if (isAjax(request)) {
      Ok(Json.toJson(Keyword.all))
    } else {
      NotFound
    }
  }

  def paperTypes = Authenticated { implicit request =>
    if (isAjax(request)) {
      Ok(Json.toJson(PaperType.all))
    } else {
      NotFound
    }
  }

  implicit val publicationShort = new Writes[Publication] {
    def writes(model: Publication) = Json.obj(
      "id" -> model.id,
      "title" -> model.title,
      "pubTypeTitle" -> model.pubTypeTitle,
      "authors" -> model.authors
    )
  }

  implicit val publicationFull = Json.writes[Publication]

  implicit val publicationsPages = new Writes[PublicationsPages] {
    def writes(model: PublicationsPages) = Json.obj(
      "publications" -> Json.toJson(model.publications)(Writes.list(publicationShort)),
      "offset" -> model.offset,
      "limit" -> model.limit,
      "hasPrev" -> model.hasPrev,
      "hasNext" -> model.hasNext
    )
  }

  implicit val searchQueryParser = new Writes[Map[String, Seq[String]]] {
    def writes(o: Map[String, Seq[String]]): JsValue = {
      var json = Json.obj()
      o.foreach(pair => pair match {
        case ("pubType", _) =>
          json = json + (pair._1 -> Json.toJson(pair._2.map(_.toInt)))
        case ("author", _) | ("keyword", _) | ("institution", _) =>
          json = json + (pair._1 -> Json.toJson(pair._2.map(_.toLowerCase)))
        case ("start_year", _) | ("end_year", _) | ("limit", _) | ("offset", _) =>
          json = json + (pair._1 -> JsNumber(pair._2.head.toInt))
        case (_, _) =>
          json = json + (pair._1 -> JsString(pair._2.head.toLowerCase))
      })
      json
    }
  }
}