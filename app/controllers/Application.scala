package controllers

import java.io.PrintStream
import java.net.{Socket, InetAddress}

import play.api.mvc._
import play.api.data._
import play.api.data.Forms._

import models._

import scala.io.BufferedSource

class Application extends Controller {

  lazy val loginForm = Form(
    tuple(
      "login" -> nonEmptyText,
      "password" -> nonEmptyText
    ) verifying("Invalid login or password", params => params match {
      case (l, p) => check(l, p)
    })
  )

  def check(login: String, password: String) = {
    AuthUser.authenticate(login, password)
  }

  def login = Action { implicit request =>
    Ok(views.html.login())
  }

  def performLogin = Action { implicit request =>
    loginForm.bindFromRequest.fold(
      formWithErrors => Redirect("/login").flashing("error" -> "Invalid login/password."),
      success = (user: (String, String)) => {
        val login: String = user._1

        Redirect("/").withSession(
          Security.username -> login
        )
      }
    )
  }

  def logout = Action {
    Redirect("/").withNewSession
  }

  def index(any: String) = AuthenticatedWithRedirect {
    Ok(views.html.index())
  }

  def connect = Action {
    SQLConnect.query("select")
    val s = new Socket(InetAddress.getByName("localhost"), 4551)
    lazy val in = new BufferedSource(s.getInputStream).getLines()
    val out = new PrintStream(s.getOutputStream)

    out.println("select A, B, C from table3;")
    out.flush()
    val l = List[String]()
    l + in.next()
    l + in.next()
    l + in.next()

    s.close()

    Ok(l.mkString("<br>"))
  }
}